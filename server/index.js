const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes');

const captains = console;
const app = express();
const port = process.env.PORT || 1337;
const www = process.env.WWW || './dist/xauruk-angular';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(www));


app.use('/api', routes);

app.get('*', (req, res) => {
    res.sendFile(`index.html`, { root: www });
});
app.listen(port, () => captains.log(`listening on http://localhost:${port}`));
