const express = require('express');
const router = express.Router();
const file = require('./maps.json');

router.get('/map', (req, res, next) => {
    res.json(file);
});

module.exports = router;