import { Injectable }  from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from './core/config';

@Injectable()
export class AppInitService {
 
    constructor(private http: HttpClient) { }
    
    Init() {
         return new Promise<void>((resolve, reject) => {
            this.http.get('/api/map').subscribe(res => {
                CONFIG.map = res;
                resolve()
            });
        });
    }
}