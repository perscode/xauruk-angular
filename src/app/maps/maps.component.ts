import { Component, OnInit } from '@angular/core';
import { CONFIG } from '../core/config';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {
  map;

  constructor() {
    this.map = JSON.stringify(CONFIG.map, null, '  ');
  }

  ngOnInit() {
  }

}
